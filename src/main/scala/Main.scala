object Main extends App {
  val todos = new Todos(Seq("Learn Scala", "Check browser usage"))
  todos.print
}

case class Todos(todos: Seq[String] = Seq()) {
  def print = println(todos)
}
